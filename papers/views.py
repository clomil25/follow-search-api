from django.shortcuts import render

# Create your views here.


from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from papers.models import Paper


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def read_paper(request):
    # TODO Read paper view and Get all papers read view or Papers read by followers view
    """
    Sets paper to read when the user selects read paper

    :param request:
    :return:
    """

    if request.method == 'POST':
        data = request.data
        try:
            paper = Paper.objects.get(doi=data.get('doi'))
        except Paper.DoesNotExist:
            return Response({"error": "doi not found"}, status=404)
        paper.read_by_users.add(request.user)
        paper.save()
        return Response({"success": "paper read"}, status=200)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def open_paper(request):
    """
    Sets a paper to unread till the user says they have finished it or it has been a long time

    :param request:
    :return:
    """

    if request.method == 'POST':
        data = request.data
        try:
            # TODO Check if user has already read?
            paper = Paper.objects.get(doi=data.get('doi'))
        except Paper.DoesNotExist:
            return Response({"error": "doi not found"}, status=404)
        paper.unread_by_user.add(request.user)
        paper.save()
        return Response({"success": "paper is in your unread list"}, status=200)
