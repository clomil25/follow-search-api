from django.urls import re_path
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'papers'

urlpatterns = (
    re_path(r"^papers/read?$", views.read_paper, name='get-followers'),
    re_path(r"^papers/open?$", views.open_paper, name='open-paper'),

)


