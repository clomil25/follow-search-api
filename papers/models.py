from django.db import models

from accounts.models import BaseAccount


class Paper(models.Model):
    doi = models.CharField(primary_key=True, max_length=32)
    read_by_users = models.ManyToManyField(BaseAccount,
                                           related_name='papers_read',
                                           blank=True)
    unread_by_user = models.ManyToManyField(BaseAccount,
                                            related_name='papers_unread',
                                            blank=True)
    abstract = models.TextField()
    title = models.CharField(max_length=150,
                             null=True,
                             blank=True)



''' 
    paper.read_by_users.add({user pk})  # add a user to this paper
    paper.read_by_users.all() # Get all users who read this paper
    
    user.papers_read.all() # Get papers read by this user
    
'''