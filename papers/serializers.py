from rest_framework import serializers

from accounts.models import BaseAccount
from accounts.serializers import BaseAccountSerializer
from papers.models import Paper


class PaperSerializer(serializers.ModelSerializer):
    """ Basic paper serializer. All fields returned"""
    class Meta:
        model = Paper
        fields = ('__all__'
                  )


class FollowedPaperSerializer(serializers.ModelSerializer):
    """ Serializer for followed papers to remove excess fields"""
    class Meta:
        model = Paper
        fields = ('abstract',
                  'title'
                  )
