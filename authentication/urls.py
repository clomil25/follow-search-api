from django.urls import re_path
from rest_framework_simplejwt import views as jwt_views

app_name = 'auth'

urlpatterns = (
    re_path(r"^auth/token/?$", jwt_views.TokenObtainPairView.as_view(), name='get-token'),
    re_path(r'^auth/token/refresh/?$', jwt_views.TokenRefreshView.as_view(), name='token-refresh'),
)