from django.db import models
from django.contrib.auth.models import AbstractUser

from .managers import CustomAccountManager


class BaseAccount(AbstractUser):
    username = None
    email = models.EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomAccountManager()

    def __str__(self):
        return self.email
