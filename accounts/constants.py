SUPERUSER = 1
STAFF = 2

ACCOUNT_TYPE_CHOICES = (
    (SUPERUSER, 'superuser',),
    (STAFF, 'staff')
)
