from rest_framework import serializers

from accounts.models import BaseAccount


class BaseAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseAccount
        fields = ('email',
                  )
