from django.urls import re_path
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'followers'

urlpatterns = (
    re_path(r"^followers/followers?$", views.get_followers, name='get-followers'),
    re_path(r"^followers/follow?$", views.follow, name='follow-user'),
    re_path(r"^followers/follower-recommendations?$", views.follower_papers, name='follower-recommendations'),

)


