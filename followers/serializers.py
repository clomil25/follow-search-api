from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError as DrfValidationError
from django.utils import timezone

from accounts.models import BaseAccount
from accounts.serializers import BaseAccountSerializer
from papers.serializers import PaperSerializer
from .models import FollowerRelationships


class FollowerRelationshipSerializer(serializers.ModelSerializer):
    follower = BaseAccountSerializer()
    followed = BaseAccountSerializer()

    class Meta:
        model = FollowerRelationships
        fields = ('follower',
                  'followed',
                  )

    def create(self, validated_data):
        validated_data['created_at'] = timezone.now()
        return FollowerRelationships.objects.create(**validated_data)


class FollowerPaperSerializer(serializers.ModelSerializer):
    followed = BaseAccountSerializer()
    # papers_read = PaperSerializer

    class Meta:
        model = FollowerRelationships
        fields = ('followed',
                  )
