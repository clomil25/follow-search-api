from django.contrib.auth.models import User
from django.forms import model_to_dict
from django.shortcuts import render
from rest_framework import viewsets, mixins
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from accounts.models import BaseAccount
from accounts.serializers import BaseAccountSerializer
from papers.models import Paper
from papers.serializers import PaperSerializer, FollowedPaperSerializer
from .models import FollowerRelationships
from .serializers import FollowerRelationshipSerializer, FollowerPaperSerializer


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_followers(request):
    """
    Returns list of all followers for auth user

    :param request:
    :return:
    """

    followers = FollowerRelationships.objects.filter(followed=request.user)
    serializer = FollowerRelationshipSerializer(followers, many=True)
    return Response(serializer.data)


@api_view(["POST", "DELETE"])
@permission_classes((IsAuthenticated,))
def follow(request):
    """
    Follow the given user

    :param request:
    :return:
    """

    if request.method == 'POST':
        # Check if user who is following is same as reqeust.user
        if not request.data.get('follower') == request.user.email:
            return Response({}, status=400)
        data = request.data

        # Set each user on Relationship model
        followed = BaseAccount.objects.get(email=data.get('followed'))
        rel = FollowerRelationships.objects.create(follower=request.user, followed=followed)
        if rel:
            serializer = FollowerRelationshipSerializer(rel)
            return Response(serializer.data, status=201)
        else:
            return Response({}, status=401)

    if request.method == 'DELETE':
        # TODO Unfollow
        pass


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def follower_papers(request):
    """
    Returns list of all papers your followers/who you follow have read

    :param request:
    :return:
    """

    # get all followers
    # TODO query so we don't get all fields. How can I optimize this
    followed = FollowerRelationships.objects.prefetch_related('followed__papers_read').filter(follower=request.user)

    papers = {}
    for rel in followed:
        user = rel.followed
        papers[user.email] = (FollowedPaperSerializer(user.papers_read.all(), many=True)).data

    return Response(papers)