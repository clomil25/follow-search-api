from django.db import models
from django.utils import timezone

from accounts.models import BaseAccount


class FollowerRelationships(models.Model):
    follower = models.ForeignKey(BaseAccount, on_delete=models.CASCADE, related_name='following')
    followed = models.ForeignKey(BaseAccount, on_delete=models.CASCADE, related_name='followers')
    created_at = models.DateField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['follower', 'followed'],
                                    name="follow_relationship")
        ]

    def save(self, **kwargs):
        self.created_at = timezone.now()
        super(FollowerRelationships, self).save(**kwargs)


